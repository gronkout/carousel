export const cel = (tag, attributes, styles) => {
	const el = typeof tag === 'string' ? document.createElement(tag) : tag;
	if (attributes) {
		for (let key in attributes) {
			el.setAttribute(key, attributes[key]);
		}
	}
	if (styles) {
		for (let key in styles) {
			el.style[key] = styles[key];
		}
	}
	return el;
}

export const move = (target, distance, speed, callback) => {
	let start = parseInt(target.style.left) || 0;
	let dest = start + distance;

	const animation = () => {
		if (distance > 0) {
			start += speed;
			if (start >= dest) {
				target.style.left = `${dest}px`;
				callback();
				return false;
			}
		} else {
			start -= speed;
			if (start <= dest) {
				target.style.left = `${dest}px`;
				callback();
				return false;
			}
		}
		target.style.left = `${start}px`;

		requestAnimationFrame(animation);
	};
	animation();
}
