import Carousel from './carousel.js';

new Carousel({
    url: '/test/carousel/json/sample.json',
    thumbnailList: '#rolling',
    thumbnailPrevBtn: '#prevBtn_list',
    thumbnailNextBtn: '#nextBtn_list',
    viewerList: '#viewer',
    ViewerPrevBtn: '#prevBtn_big',
    ViewerNextBtn: '#nextBtn_big'
});

