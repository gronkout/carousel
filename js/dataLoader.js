const DataLoader = (() => {
    let _url;

    const getData = (url) => {
        const xhr = new XMLHttpRequest();

        return new Promise((resolve, reject) => {
            xhr.open('get', url);

            xhr.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    resolve(xhr.response);
                } else {
                    reject({ status: this.status, statusText: xhr.statusText });
                }
            };

            xhr.onerror = function () {
                reject({ status: this.status, statusText: xhr.statusText });
            };

            xhr.send();
        });
    }

    return class {
        constructor (url) {
            if (!url || typeof url !== 'string') throw 'invalid';

            _url = url;
        }

        async getJson () {
            try {
                const data = await getData(_url);
                return JSON.parse(data);
            } catch (error) {
                throw error;
            }
        }
    }
})();

export default DataLoader;

