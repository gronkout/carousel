import DataLoader from './dataLoader.js';
import { cel, move } from './util.js'

const Carousel = (() => {
	let _option = {};
	let _data = null;
	let _prevIndex = 0;
	let _activeIndex = 0;

	const _execute = async () => {
		const { url } = _option;
		_data = await new DataLoader(url).getJson();

		makeThumbnailList();
		makeViewerList();
	}

	const makeThumbnailList = () => {
		const { thumbnailList, thumbnailPrevBtn, thumbnailNextBtn } = _option;
		const { imageCount, items } = _data;

		// 썸네일 리스트
		const thumbnailListMaskElement = document.querySelector(thumbnailList);
		const thumbnailListMaskWidth = thumbnailListMaskElement.offsetWidth;
		const thumbnailListElement = cel('ul');

		items.forEach((item, index) => {
			const liElement = cel('li', { 'data-index': index });
			const aElement = cel('a', { href: 'javascript:void(0)' });
			const spanElement = cel('span');
			const imgElement = cel('img', { width: '100%', height: '100%', src: item.thumbURL, alt: item.imgDesc });

			aElement.appendChild(spanElement);
			aElement.appendChild(imgElement);
			liElement.appendChild(aElement);
			thumbnailListElement.appendChild(liElement);
		});
		thumbnailListMaskElement.appendChild(thumbnailListElement);

		// 썸네일 리스트 인터렉션
		const thumbnailListActive = () => {
			thumbnailListElement.childNodes[_prevIndex].children[0].children[0].style.display = '';
			thumbnailListElement.childNodes[_activeIndex].children[0].children[0].style.display = 'block';
		}
		thumbnailListActive();

		const onClickThumbnailList = (event) => {
			event.preventDefault();
			_prevIndex = _activeIndex;
			_activeIndex = parseInt(event.target.parentNode.parentNode.getAttribute('data-index'));
			thumbnailListActive();
		}

		thumbnailListElement.addEventListener('click', onClickThumbnailList);

		// 썸네일 좌우 버튼 인터렉션
		const prevBtnElement = document.querySelector(thumbnailPrevBtn);
		const nextBtnElement = document.querySelector(thumbnailNextBtn);
		const totalPage = Math.floor(imageCount / 10);
		let currentPage = 0;
		let moving = false;

		const moveCallback = () => {
			moving = false;
			_prevIndex = _activeIndex;
			_activeIndex = currentPage * 10;
			thumbnailListActive();
		}

		prevBtnElement.addEventListener('click', (event) => {
			event.preventDefault();
			if (currentPage > 0 && !moving) {
				moving = true;
				currentPage--;
				move(thumbnailListElement, thumbnailListMaskWidth, 40, moveCallback);
			}
		});
		nextBtnElement.addEventListener('click', (event) => {
			event.preventDefault();
			if (currentPage < totalPage && !moving) {
				moving = true;
				currentPage++;
				move(thumbnailListElement, -thumbnailListMaskWidth, 40, moveCallback);
			}
		});
	}

	const makeViewerList = () => {
		const { viewerList, ViewerPrevBtn, ViewerNextBtn } = _option;
		const { imageCount, items } = _data;

		// 뷰어 리스트
		const viewerListMaskElement = document.querySelector(viewerList);
		const viewerListMaskWidth = viewerListMaskElement.offsetWidth;
		const viewerListElement = cel('ul', {}, { width: '100000px' });

		items.forEach((item, index) => {
			const liElement = cel('li', { 'data-index': index });
			const imgElement = cel('img', { width: '100%', height: '100%', src: item.viewURL, alt: item.imgDesc });

			liElement.appendChild(imgElement);
			viewerListElement.appendChild(liElement);
		});
		viewerListMaskElement.appendChild(viewerListElement);

		// 뷰어 좌우 버튼 인터렉션
		const prevBtnElement = document.querySelector(ViewerPrevBtn);
		const nextBtnElement = document.querySelector(ViewerNextBtn);
		const totalPage = imageCount - 1;
		let currentPage = 0;
		let moving = false;

		const moveCallback = () => {
			moving = false;
		}

		prevBtnElement.addEventListener('click', () => {
			if (currentPage > 0 && !moving) {
				moving = true;
				currentPage--;
				move(viewerListElement, viewerListMaskWidth, 60, moveCallback);
			}
		});
		nextBtnElement.addEventListener('click', () => {
			if (currentPage < totalPage && !moving) {
				moving = true;
				currentPage++;
				move(viewerListElement, -viewerListMaskWidth, 60, moveCallback);
			}
		});
	}

	return class {
		constructor (option) {
			const { thumbnailList, thumbnailPrevBtn, thumbnailNextBtn, viewerList } = option;

			if (!thumbnailList || typeof thumbnailList !== 'string') throw 'invalid';
			if (!thumbnailPrevBtn || typeof thumbnailPrevBtn !== 'string') throw 'invalid';
			if (!thumbnailNextBtn || typeof thumbnailNextBtn !== 'string') throw 'invalid';
			if (!viewerList || typeof viewerList !== 'string') throw 'invalid';

			_option = option;

			_execute();
		}
	}
})();

export default Carousel;
